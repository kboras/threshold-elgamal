import threshold_elgamal.util
from threshold_elgamal.elgamal import run_tc_scheme
from threshold_elgamal.util import reconstruct_polynomial, construct_random_polynomial, polynomial

# for _ in range(50):
#     run_tc_scheme(4, 5, 10)

# for _ in range(5):
#     print(threshold_elgamal.util.get_generator(1997, 499))

res = run_tc_scheme(3, 5, 10)
print(res)
#
# q = threshold_elgamal.util.find_prime(0, 1000000)
# p = threshold_elgamal.util.find_p(q)
# g = threshold_elgamal.util.get_generator(p, q)
# h = threshold_elgamal.util.get_generator(p, q)
#
# print(p, q, g, h)

