
Util
================

.. currentmodule:: threshold_elgamal.util

.. autofunction:: threshold_elgamal.util.get_new_params

.. autofunction:: threshold_elgamal.util.calculate_lagrange_coeff

.. autofunction:: threshold_elgamal.util.construct_random_polynomial

.. autofunction:: threshold_elgamal.util.polynomial

.. autofunction:: threshold_elgamal.util.reconstruct_polynomial
