.. Threshold ElGamal documentation master file, created by
   sphinx-quickstart on Mon May  9 20:07:51 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Threshold ElGamal's documentation!
=============================================

This is a library that provides methods for threshold encryption, decryption and DKG (distributed key generation) algorithm using the ElGamal cryptosystem. Threshold ElGamal encryption and decryption algorithms were based on Cachin's `work <https://cachin.com/cc/sft12/distcrypto.pdf>`_, and the algorithm used for distributed key pair generation was based on a 1999 `paper <https://link.springer.com/chapter/10.1007/3-540-48910-X_21>`_ by Gennaro et al. Both algorithms use a form of secret sharing first introduced in a 1979 `paper <https://dl.acm.org/doi/abs/10.1145/359168.359176>`_ by Shamir.

This goes without saying, but you **should NOT use this library in a production setting**. It has not been pentested nor was any kind of exhaustive security analysis done. This library is developed primarily for demonstration of theoretic threshold schemes.

Installation
------

Before installing the library, make sure its dependencies are installed: :code:`pycryptodome`, :code:`gmpy2` and :code:`setuptools`. The library is written for Python >= 3.9.

You can then install the library via :code:`pip`:

::

   pip install threshold-elgamal

Getting started
-----
The simplest way you can test out this library is by running its main function:

::

   from threshold_elgamal import run_tc_scheme

   res = run_tc_scheme(k=3, n=5, m=10)
   if res is True:
       print("Success!")


You can also create your own threshold scheme manually, and then encrypt and decrypt a message of your choosing:

::

   from threshold_elgamal import create_tc_scheme

   message = 10
   public_key, players, scheme = create_tc_scheme(k=3, n=5)
   c1, c2 = scheme.encrypt(public_key, message=message)
   decryption_shares = {player.id: player.get_decryption_share(c1) for player in players}
   decrypted_msg = scheme.decrypt(c2, decryption_shares)

   if decrypted_msg == message:
       print("Success!")



.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. toctree::
   :maxdepth: 2
   :caption: Python API:

   api/telgamal
   api/player
   api/util



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
